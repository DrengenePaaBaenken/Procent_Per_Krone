﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
namespace PPK
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            Button udregn_button = FindViewById<Button>(Resource.Id.udregn);
            udregn_button.Click += udregnOnClick;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void udregnOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            TextView harboe_view = FindViewById<TextView>(Resource.Id.harboe);
            EditText vaegt_view = FindViewById<EditText>(Resource.Id.vaegt);
            EditText promille_view = FindViewById<EditText>(Resource.Id.promille);
            EditText tidsperiode_view = FindViewById<EditText>(Resource.Id.tidsperiode);
            TextView hph_view = FindViewById<TextView>(Resource.Id.hph);
            double.TryParse(vaegt_view.Text, out double vaegt);
            double.TryParse(promille_view.Text, out double promille);
            double.TryParse(tidsperiode_view.Text, out double tidsperiode);
            double harboe = (1.131728693d * promille / (Math.Pow(0.983351d, vaegt)) * 2.9 / 3);//AFRUND  TALLET TIL ET INTEGER.
            harboe_view.Text = (1.131728693d*promille/(Math.Pow(0.983351d,vaegt))*2.9/3).ToString();
            hph_view.Text = ((harboe + 1 * tidsperiode) / tidsperiode).ToString();
        }
	}
}

